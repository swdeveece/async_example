﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace ASYNC_Example
{
    class Program
    {
        

        static void Main(string[] args)
        {

            //Serial Approach
            Stopwatch sw = Stopwatch.StartNew();

            int carId = BookCar();
            int hotelId = BookHotel();
            int planId = BookPlane();

            

            Console.WriteLine("Finished in {0} sec.", sw.ElapsedMilliseconds / 1000.0);
            Console.Read();

            //Threading - Main thread exits prematurely
            //Stopwatch sw = Stopwatch.StartNew();


            //Task<int> carTask = Task.Factory.StartNew<int>(BookCar);
            //Task<int> hotelTask = Task.Factory.StartNew<int>(BookHotel);
            //Task<int> planeTask = Task.Factory.StartNew<int>(BookPlane);

            //Console.WriteLine("Finished in {0} sec.", sw.ElapsedMilliseconds / 1000.0);
            //Console.Read();

            //Threading - Force Main thread to wait for tasks
            //Stopwatch sw = Stopwatch.StartNew();


            //Task<int> carTask = Task.Factory.StartNew<int>(BookCar);
            //Task<int> hotelTask = Task.Factory.StartNew<int>(BookHotel);
            //Task<int> planeTask = Task.Factory.StartNew<int>(BookPlane);

            //Task.WaitAll(carTask, hotelTask, planeTask);


            ////Capture Task return
            //Console.WriteLine("car = {0}, plane = {1}, hotel = {2}", carTask.Result, planeTask.Result, hotelTask.Result);


            //Console.WriteLine("Finished in {0} sec.", sw.ElapsedMilliseconds / 1000.0);
            //Console.Read();


            //Using ASYNC

            //Running();

            //for (int i = 0; i < 100; i++)
            //{

            //    Thread.Sleep(500);
            //    Console.WriteLine(i);
            //}
            //Console.Read();


        }
        static Random rand = new Random();

        private static int BookPlane()
        {
                
               Console.WriteLine("Booking plane...");
               Thread.Sleep(5000);
               Console.WriteLine("Done with plane.");
               return rand.Next(100);
           

        }

        private static int BookHotel()
        {
            Console.WriteLine("Booking hotel...");
            Thread.Sleep(8000);
            Console.WriteLine("Done with hotel.");
            return rand.Next(100);
        }

        private static int BookCar()
        {
            Console.WriteLine("Booking car...");
            Thread.Sleep(3000);
            Console.WriteLine("Done with car.");
            return rand.Next(100);
        }



        //AWAIT methods
        private static async void Running()
        {
            Stopwatch sw = Stopwatch.StartNew();

            await BookPlaneTask();

            await BookHotelTask();

            await BookCarTask();

            Console.WriteLine("Finished in {0}seconds", sw.ElapsedMilliseconds / 1000);
            //BookCarTask();
            //BookPlaneTask();
            //BookHotelTask();


        }

        private static Task BookPlaneTask()
        {
            return Task.Factory.StartNew( () =>
            {
                Console.WriteLine("Booking car...");
                Thread.Sleep(3000);
                Console.WriteLine("Done with car.");
            });



        }

        private static Task BookCarTask()
        {
            return Task.Factory.StartNew(BookCar);

        }

        private static Task BookHotelTask()
        {
            return Task.Factory.StartNew(BookHotel);

        }



    }
}
